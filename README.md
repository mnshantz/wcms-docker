# WCMS Docker setup

You will need to get Docker (https://www.docker.com/products/docker-desktop) either Docker Desktop CE for Windows/Mac or Docker Toolbox for Windows (https://docs.docker.com/toolbox/toolbox_install_windows/) or Mac (https://docs.docker.com/toolbox/toolbox_install_mac/).
If you plan to run VirtualBox and Docker you will need to install the Docker Toolbox, Docker Desktop CE requires Hyper-V to be enabled which isn't compatible with VirtualBox. We recommend using Docker Desktop.

**CAUTION!** This Docker box is meant for local development only. This set up is not hardened and is not secure, so
only us it as a local development environment.

You will need to create a Docker ID (if you don't already have one) to download the latest Docker Desktop.

During setup you will be asked if you want to use Linux or Windows containers, for consistency (between Mac and Windows users) we will choose linux containers.

**Note** If you have VirtualBox installed you will get a warning:
"Hyper-V and Containers are not enabled. Do you want to enable them for Docker to be able to work properly? Your computer will restart automatically. Note: VirtualBox wil no longer work."

We want to enable Hyper-V so you can accept this (but as it said Virtualbox will no longer work). 

When your computer restarts, Docker should also start and it will be asking you to log in with your Docker ID. If you don't want to log in, you can choose Privacy settings and the uncheck "send usage statistics..." (You can also optionally uncheck "start Docker Desktop when you log in"

**Note** If you had Docker Toolbox installed you should uninstall it and you might need to remove User Environmental Variables. 
If this is the case, you need to go to System > System Info > Advanced Settings > Environmental Variables and remove and Docker settings from the User Variables.
Once you've done this you should reboot your machine.

You should now be ready to clone the repo:


```bash
       git clone https://git.uwaterloo.ca/mnshantz/wcms-docker.git wcms-docker
```

Go into the directory you cloned Docker into:

```bash
       cd wcms-docker
```

First we need to build the images,(this takes about 12 minutes or so to build):

```bash
       docker-compose build 
```
**Note** The first time you run this, you may be asked to share C: drive and you may need to add a Windows Firewall permission. Click yes/okay for both.

Once that's done you can create the containers:

```bash
       docker-compose up -d
```
If you get an error running docker-compose related to `wincred` go to `~/.docker/config.json` and remove the `"credstore":"wincred"` but leave the curly braces `{}` and save the file.

To check that all the boxes are running:

```bash
       docker-compose ps
```
If all containers say up, you are good to proceed to the next step.

Next we will need to run the profile rebuild for either our Drupal 7 or Drupal 8 container:

go to the URL: http://docker:9000 

You can use Portainer to easily log in to the web container to run the profile rebuild the base profile or run composer etc...

Go to Containers (http://docker:9000/#/containers) and under "Quick Actions" look for the console icon, it looks like this `>_`

On the Drupal 7 container you can log in as root on the Drupal 8 container change the log in to vagrant.

Once in, you can update the profile as you would using wcms-vagrant.


On Windows, if you get the error:

```bash
        un\\\\docker.sock:/var/run/docker.sock"\n is not a valid Windows path'

ERROR: for portainer  Cannot create container for service portainer: b'Mount denied:\nThe source path "\\\\var\\\\run\\\\docker.sock:/var/run/docker.sock"\nis not a valid Windows path'
Encountered errors while bringing up the project.
```

You will need to use Windows Powershell (as Admin) and enter:
```bash
        $Env:COMPOSE_CONVERT_WINDOWS_PATHS=1
```

Potential issue with Firefox and being able to resolve localhost:
https://www.reddit.com/r/sysadmin/comments/94r885/firefox_will_soon_be_sending_all_dns_requests_to/

Fix (go to about:config in Firefox URL bar):
change ```network.trr.mode=0``` to network.trr.mode=5```