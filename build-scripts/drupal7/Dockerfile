############################################################
# Dockerfile to build WCMS container images
# Based on Ubuntu
############################################################

# Set the base image to Ubuntu
FROM ubuntu:xenial

# Pass the argument variables from the docker-compose file to the Dockerfile and echo them out
ARG PHP_VERSION
ARG DRUPAL7_VERSION
ARG PROJECT_BASE_URL

RUN echo "PHP version: $PHPVERSION"
RUN echo "Drupal 7 version: $DRUPAL7_VERSION"
RUN echo "Project base URL: $PROJECT_BASE_URL"

# Allows installing of packages without prompting the user to answer any questions
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && \
    apt-get install --assume-yes \
    software-properties-common \
    language-pack-en \
    curl \
    apt-transport-https

## Add the repo to get the latest PHP versions
RUN apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
RUN export LANG=en_US.UTF-8

## Need LC_ALL= otherwise adding the repos throws an ascii error.
RUN LC_ALL=en_US.UTF-8 add-apt-repository -y ppa:ondrej/php

## Add the git repo so we can get the latest git (we need 2.9.2+)
RUN add-apt-repository ppa:git-core/ppa

RUN apt-get update

# Added so we can install 8.x branch of nodejs.
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -

# Install packages.
RUN apt-get install -y \
    vim \
    git \
    apache2 \
    php${PHP_VERSION} \
    php${PHP_VERSION}-apc \
    php${PHP_VERSION}-fpm \
    php${PHP_VERSION}-xml \
    php${PHP_VERSION}-simplexml \
    php${PHP_VERSION}-mbstring \
    php${PHP_VERSION}-cli \
    php${PHP_VERSION}-mysql \
    php${PHP_VERSION}-gd \
    php${PHP_VERSION}-gd \
    php${PHP_VERSION}-curl \
    php${PHP_VERSION}-ldap \
    php${PHP_VERSION}-mcrypt \
    php${PHP_VERSION}-zip \
    php-pear \
    libapache2-mod-php${PHP_VERSION} \
    optipng \
    pdftk \
    jpegoptim \
    imagemagick \
    libapache2-mod-fcgid \
    libapache2-mod-fastcgi \
    curl \
    mysql-client \
    openssh-server \
    wget \
    ruby-sass \
    ruby-compass \
    nodejs \
    dos2unix \
    supervisor
RUN apt-get clean
## enable rewrite and ssl for apache2
RUN a2enmod rewrite
RUN a2enmod ssl
## for Content Security Policy (CSP).
RUN a2enmod headers
## fcgid needed to run multiple versions of PHP under the same apache.
## RUN a2enmod fcgid
## enable mcrypt
RUN phpenmod mcrypt
## add upload progress
RUN apt-get install php-uploadprogress
# Install Composer.
RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer
# Install Drush 7.
RUN composer global require drush/drush:8.*
RUN composer global update
# Unfortunately, adding the composer vendor dir to the PATH doesn't seem to work. So:
RUN ln -s /root/.composer/vendor/bin/drush /usr/local/bin/drush

# Manually set up the apache environment variables
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid

## From https://www.digitalocean.com/community/tutorials/how-to-create-a-ssl-certificate-on-apache-for-ubuntu-14-04
RUN cd /tmp
RUN openssl req -x509 -nodes -days 1825 -newkey rsa:2048 -keyout /tmp/apache.key -out /tmp/apache.crt \
-subj "/C=CA/ST=ON/L=Waterloo/O=uWaterloo/OU=IST/CN=wcms-devsite/emailAddress=localhost@example.com"
RUN cp /tmp/apache.crt /etc/ssl/certs/server.crt
RUN cp /tmp/apache.key /etc/ssl/private/server.key

## Add the "vagrant" user (because we are all familiar with having it)
RUN useradd -d /home/vagrant -ms /bin/bash -g root -G sudo -p vagrant vagrant

# Replace the default site configuration with our own.
COPY 000-default.conf /etc/apache2/sites-available/000-default.conf
RUN a2enmod proxy_fcgi setenvif
RUN a2enconf php${PHP_VERSION}-fpm
RUN service php${PHP_VERSION}-fpm restart

## Make sure we are running php we selected
RUN update-alternatives --set php /usr/bin/php${PHP_VERSION}
RUN a2enmod php${PHP_VERSION}
RUN service apache2 restart

## Check version of PHP
RUN php -v

## Install Drupal.
RUN drush dl -v -d drupal-${DRUPAL7_VERSION} --destination="/var/www" --drupal-project-rename="drupal7"

RUN	chown -R www-data:www-data /var/www/

## Install the drush registry_rebuild module
RUN drush @none dl registry_rebuild-7.x

## Create the config folder in drupal root and an empty password_settings file
RUN mkdir /var/www/drupal7/config
RUN touch /var/www/drupal7/config/password_settings.php

## Create an empty fonts folder so I know where it's supposed to go! ;)
RUN mkdir /var/www/drupal7/fonts

RUN cd /var/www/drupal7 && \
    ln -s . fdsu1 && \
    ln -s . fdsu2 && \
    ln -s . fdsu3 && \
    ln -s . fdsu4

## Create site folders for drupal multi-site
RUN mkdir /var/www/drupal7/sites/${PROJECT_BASE_URL}.fdsu1 
RUN mkdir /var/www/drupal7/sites/${PROJECT_BASE_URL}.fdsu1/files 
RUN mkdir /var/www/drupal7/sites/${PROJECT_BASE_URL}.fdsu1/files/temp
RUN mkdir /var/www/drupal7/sites/${PROJECT_BASE_URL}.fdsu1/modules
## Create settings.php file
COPY settings.conf /var/www/drupal7/sites/${PROJECT_BASE_URL}.fdsu1/settings.php
RUN mkdir /var/www/drupal7/sites/${PROJECT_BASE_URL}.fdsu2 
RUN mkdir /var/www/drupal7/sites/${PROJECT_BASE_URL}.fdsu2/files
RUN mkdir /var/www/drupal7/sites/${PROJECT_BASE_URL}.fdsu2/files/temp
RUN mkdir /var/www/drupal7/sites/${PROJECT_BASE_URL}.fdsu2/modules
## Create settings.php file
COPY settings.conf /var/www/drupal7/sites/${PROJECT_BASE_URL}.fdsu2/settings.php
## Change sitename to match site
RUN cd /var/www/drupal7/sites/${PROJECT_BASE_URL}.fdsu2 && \
    sed -i 's/fdsu1/fdsu2/g' settings.php
RUN mkdir /var/www/drupal7/sites/${PROJECT_BASE_URL}.fdsu3
RUN mkdir /var/www/drupal7/sites/${PROJECT_BASE_URL}.fdsu3/files
RUN mkdir /var/www/drupal7/sites/${PROJECT_BASE_URL}.fdsu3/files/temp
RUN mkdir /var/www/drupal7/sites/${PROJECT_BASE_URL}.fdsu3/modules
## Create settings.php file
COPY settings.conf /var/www/drupal7/sites/${PROJECT_BASE_URL}.fdsu3/settings.php
## Change sitename to match site
RUN cd /var/www/drupal7/sites/${PROJECT_BASE_URL}.fdsu3 && \
    sed -i 's/fdsu1/fdsu3/g' settings.php
RUN mkdir /var/www/drupal7/sites/${PROJECT_BASE_URL}.fdsu4
RUN mkdir /var/www/drupal7/sites/${PROJECT_BASE_URL}.fdsu4/files
RUN mkdir /var/www/drupal7/sites/${PROJECT_BASE_URL}.fdsu4/files/temp
RUN mkdir /var/www/drupal7/sites/${PROJECT_BASE_URL}.fdsu4/modules
## Create settings.php file
COPY settings.conf /var/www/drupal7/sites/${PROJECT_BASE_URL}.fdsu4/settings.php
## Change sitename to match site
RUN cd /var/www/drupal7/sites/${PROJECT_BASE_URL}.fdsu4 && \
    sed -i 's/fdsu1/fdsu4/g' settings.php
## Get the base profile from git
RUN git clone https://git.uwaterloo.ca/wcms/uw_base_profile.git /var/www/drupal7/profiles/uw_base_profile
RUN chown -R vagrant:www-data /var/www/drupal7/profiles/uw_base_profile
RUN chmod -R g+w /var/www/drupal7/profiles/uw_base_profile
## Rebuild the profile to get the latest modules
#RUN cd /var/www/drupal7/profiles/uw_base_profile
#RUN /var/www/drupal7/profiles/uw_base_profile/rebuild.sh

## Add the Servername to the apache2 conf file.
## RUN echo "ServerName ${PROJECT_BASE_URL}" >> /etc/apache2/apache2.conf

# Copy our custom entrypoint and make it executable.
COPY docker-entrypoint-d7.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint-d7.sh

## Add Drupal specific changes for PHP to the php.ini files.
RUN sed -i "s/display_errors = .*/display_errors = On/" /etc/php/${PHP_VERSION}/apache2/php.ini && \
    sed -i "s/display_errors = .*/display_errors = On/" /etc/php/${PHP_VERSION}/fpm/php.ini && \
    sed -i "s/display_errors = .*/display_errors = On/" /etc/php/${PHP_VERSION}/cli/php.ini && \
    sed -i "s/max_execution_time =.*/max_execution_time = 240/" /etc/php/${PHP_VERSION}/apache2/php.ini && \
    sed -i "s/max_execution_time =.*/max_execution_time = 240/" /etc/php/${PHP_VERSION}/cli/php.ini && \
    sed -i "s/max_input_time = .*/max_input_time = 240/" /etc/php/${PHP_VERSION}/apache2/php.ini && \
    sed -i "s/max_input_time = .*/max_input_time = 240/" /etc/php/${PHP_VERSION}/cli/php.ini && \
    sed -i "s/memory_limit = .*/memory_limit = 768M/" /etc/php/${PHP_VERSION}/apache2/php.ini && \
    sed -i "s/memory_limit = .*/memory_limit = 768M/" /etc/php/${PHP_VERSION}/cli/php.ini && \
    sed -i "s/; max_input_vars = .*/max_input_vars = 10000/" /etc/php/${PHP_VERSION}/apache2/php.ini && \
    sed -i "s/;date.timezone = */date.timezone = America\/Toronto/" /etc/php/${PHP_VERSION}/apache2/php.ini && \
    sed -i "s/;date.timezone = */date.timezone = America\/Toronto/" /etc/php/${PHP_VERSION}/cli/php.ini
    
COPY servername.conf /etc/apache2/conf-available
RUN a2enconf servername


# Expose the default Apache port.
EXPOSE 80
EXPOSE 443

# Replace the standard entrypoint /bin/sh with our script.
ENTRYPOINT ["docker-entrypoint-d7.sh"]

# If no command is passed to the container, start Apache by default.
CMD ["apachectl", "-D", "FOREGROUND"]
