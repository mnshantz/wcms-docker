#!/usr/bin/env bash

# Any background job should have it's messages printed.
set -m

# Don't continue if any command in the script fails.
set -e

# Delete any previously existing run file.
if [ -f /run/apache2/apache2.pid ]; then
    rm /run/apache2/apache2.pid
fi

# Allow the Apache docroot to be overridden.
APACHE_DOCROOT_DIR="${APACHE_DOCROOT_DIR:-/var/www/drupal7os}"
if [ -n "$APACHE_DOCROOT_DIR" ]; then
     sed -i 's@^\s*DocumentRoot.*@'"        DocumentRoot ${APACHE_DOCROOT_DIR}"'@'  /etc/apache2/sites-available/000-default.conf
fi

# Allow the site name to be overriden.
APACHE_SITE_NAME="${APACHE_SITE_NAME:-wcms-docker}"
if [ -n "$APACHE_SITE_NAME" ]; then
     sed -i 's@^\s*ServerName.*@'"        ServerName ${APACHE_SITE_NAME}"'@'  /etc/apache2/sites-available/000-default.conf
fi

# Allow for site aliases to be provided.
APACHE_SITE_ALIAS="${APACHE_SITE_ALIAS:-wcms-docker}"

if [ -n "$APACHE_SITE_ALIAS" ]; then
     sed -i 's@^\s*ServerAlias.*@'"        ServerAlias ${APACHE_SITE_ALIAS}"'@'  /etc/apache2/sites-available/000-default.conf
fi

## Change default PHP to 5.6 (just to be sure).
#a2dismod php7.2 && a2enmod php5.6 && update-alternatives --set php /usr/bin/php5.6 && service apache2 restart

## Change permissions on /var/www/drupal7os
chown -R vagrant:www-data /var/www/drupal7os
chmod -R g+w /var/www/drupal7os

# Now that we're set up, run whatever command was passed to the entrypoint.
exec "$@"
